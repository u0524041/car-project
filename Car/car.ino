//#include <HCSR04.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include "Adafruit_VL53L0X.h"
#define KS103ADD  0x74
Adafruit_VL53L0X lox = Adafruit_VL53L0X();
SoftwareSerial mySerial(1, 0);
//HCSR04 hc(13, 12);//initialisation class HCSR04 (trig pin , echo pin)

int brakedistance, geardistance = 0;
//char gearstalls;
//const int gearpot = 3;
const int HandBrake = 4;
const int LeftPin = 5;
const int RightPin = 6;
const int Baba = 7;
const int MainBeam = 8;
const int SmallBeam = 9;
const int INT = 10;
const int Low = 11;
const int High = 12;
const int Wash = 13;
int savegear =0;

int State_I, State_Low, State_H, State_L, State_R, State_M, State_S, State_W, State_HB, State_B = 0;

void setup()
{
  Wire.begin();
  lox.begin();
  Serial.begin(9600);
  Wire.beginTransmission(KS103ADD);
  Wire.write(byte(0x02));
  Wire.write(0x71);   // 发送降噪指令
  Wire.endTransmission();
  mySerial.begin(9600);
  pinMode(1 , OUTPUT);
  pinMode(0 , INPUT);
  pinMode(INT , INPUT);
  pinMode(Low , INPUT);
  pinMode(High , INPUT);
  pinMode(Wash , INPUT);
  pinMode(LeftPin , INPUT);
  pinMode(RightPin , INPUT);
  pinMode(MainBeam , INPUT);
  pinMode(SmallBeam , INPUT);
  pinMode(HandBrake , INPUT);
  pinMode(Baba , INPUT);
  pinMode(A3,INPUT);
  delay(1);

}

void loop()
{
  
  KS103_read();
  VL53L0X_RangingMeasurementData_t measure;
  lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
  Serial.print("{\"ThrottlePedal\":");
  Serial.print(throttlepercent(measure.RangeMilliMeter));
  //Serial.print(":");
  //Serial.print(measure.RangeMilliMeter);
  Serial.print(",\"BrakePedal\":");
  Serial.print(brakepercent(brakedistance));
  //Serial.print(":");
  //Serial.print(brakedistance);
  //gearrange(hc.dist());
  TurnSignal();
  Wipe();
  HandBrakef();
  Babaf();
  Serial.println("}");
  delay(1);
}
//KS103濾波除錯
word KS103_read() { 
  Wire.beginTransmission(KS103ADD);
  Wire.write(byte(0x02));
  Wire.write(0xb0);     //量程设置为5m 不带温度补偿
  Wire.endTransmission();
  delay(1);
  Wire.beginTransmission(KS103ADD);
  Wire.write(byte(0x02));
  Wire.endTransmission();
  Wire.requestFrom(KS103ADD, 4);
  if (4 <= Wire.available())
  {
    brakedistance = Wire.read();
    brakedistance =  brakedistance << 8;
    brakedistance |= Wire.read();
  }
}
//油門百分比轉換Function
inline long throttlepercent(int x) { 
  int tmax = 76;//sensor to throttle 最大距離
  int tmix = 33;//sensor to throttle 最小距離
  int throttlerange = (tmax - tmix); //sensor to throttle - mix
  int throttlepers = 0;
  if (x <= tmax) {
    throttlepers = (100 - (((x - tmix) * 100) / throttlerange)); //distance change to persent
    if (throttlepers <= 100) { //until full throttle
      return throttlepers;
    }
    else {
      return 100;
    }
  } else {
    return 0;
  }
}
//煞車百分比轉換function
inline int brakepercent(int y) { 
  int bmax = 230;//sensor to brake 最大距離
  int bmix = 32;//sensor to brake 最小距離
  int brakerange = (bmax - bmix); //sensor to brake - mix
  if (y <= bmax) {
    y = (100 - (((y - bmix) * 100) / brakerange)); //distance change to persent
    if (y <= 100) { //until full brake
      return y;
    }
    else {
      return 100;//最大值限制
    }
  } else {
    return 0;//最小值限制
  }
}
//排檔測距Function
/*inline  char gearrange(double dis) {
  Serial.print(",\"Gear\":");
  int g = dis*10;
  //Serial.print(g);
  if( g >= 50 && g <= 120 ){
    Serial.print("\"P\"");//P檔距離範圍
    savegear = g;
  }
  else if( g >= 121 && g <= 155 ){
    Serial.print("\"R\"");//R檔距離範圍
    savegear = g;
  }
  else if( g >= 156 && g <= 181 ){
    Serial.print("\"N\"");//N檔距離範圍
    savegear = g;
  }
  else if( g >= 181 && g <= 1000){
    Serial.print("\"D\"");//D檔距離範圍
    savegear = g;
  }
  else
  {
    //Serial.print("\"N\"");//如果有雜質一律打N
    if( savegear >= 50 && savegear <= 160 ){
      Serial.print("\"P\"");}//P檔距離範圍
    else if( savegear >= 121 && savegear <= 155 ){
      Serial.print("\"R\"");}//R檔距離範圍
    else if( savegear >= 156 && savegear <= 181 ){
      Serial.print("\"N\"");}//N檔距離範圍
    else if( savegear >= 181 && savegear <= 1000){
      Serial.print("\"D\"");}//D檔距離範圍
  }
}*/
//雨刷開關
inline int Wipe() {  
  State_I = digitalRead(INT);
  State_Low = digitalRead(Low);
  State_H = digitalRead(High);
  State_W = digitalRead(Wash);
  Serial.print(",\"Wipers\":");
  if (State_I == HIGH) {
    Serial.print(1); //雨刷-單次
  }
  else if (State_Low == HIGH) {
    Serial.print(10); //雨刷-小
  }
  else if (State_H == HIGH) {
    Serial.print(100); //雨刷-大
  }
  else if (State_W == HIGH) {
    Serial.print(1000); //雨刷-噴水清洗
  }
  else {
    Serial.print(0); //雨刷-關
  }
}
//方向燈以及大燈開關
inline int TurnSignal() { 
  State_L = digitalRead(LeftPin);
  State_R = digitalRead(RightPin);
  State_M = digitalRead(MainBeam);
  State_S = digitalRead(SmallBeam);
  Serial.print(",\"TurnSignal\":");
  if (State_L == HIGH) {
    Serial.print("\"L\""); //左轉燈
  }
  else if (State_R == HIGH) {
    Serial.print("\"R\""); //右轉燈
  }
  else {
    Serial.print("\"N\""); //常態無燈號
  }

  Serial.print(",\"Headlights\":");
  if (State_M == HIGH) {
    Serial.print(10); //遠燈
  }
  else if (State_S == HIGH) { //小燈
    Serial.print(1);
  }
  else {
    Serial.print(0); //無燈號
  }
  delay(5);
}
//手煞車開關
inline int HandBrakef() { 
  State_HB = digitalRead(HandBrake);
  Serial.print(",\"HandBrake\":");
  if (State_HB == HIGH) {
    Serial.print(0); //手煞車拉起
  } else {
    Serial.print(1); //手煞車放下
  }
}
//喇叭開關
inline int Babaf() { 
  State_B = digitalRead(Baba);
  Serial.print(",\"Baba\":");
  if (State_B == HIGH) {
    Serial.print(1);//按喇叭
  } else {
    Serial.print(0);//常態
  }
}

